from azalspy.health.helpers import *

###################################################################################################

class SacredText(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class AdalaItem(BaseItem):
    fqdn = scrapy.Field()
    link = scrapy.Field()
    prnt = scrapy.Field()

    name = scrapy.Field()
    unit = scrapy.Field()

    docx = scrapy.Field()
    p_ar = scrapy.Field()
    p_fr = scrapy.Field()

